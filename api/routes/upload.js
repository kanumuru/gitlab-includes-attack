const express = require('express')
const router = express.Router()
const upload = require('../controllers/uploadFile')
const uploadSecure = require('../controllers/secureUploadFile')

router.route('/')
    .post(upload)

router.route('/secure')
    .post(uploadSecure)

module.exports = router