import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import axios from 'axios'
import {useState} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'

function Secure() {
    const [msg, setMsg] = useState('')
    const register = event => {
        event.preventDefault()
        const e = event.target
        const data = {
            "tax-filing": {
                "name": e.name.value,
                "email": e.email.value,
                "pan": e.pan.value,
                "investments": {
                    "mutual-funds": e.funds.value === ''? 0 : parseInt(e.funds.value),
                    "insurance": e.insurance.value === ''? 0 : parseInt(e.insurance.value),
                    "fixed-deposit": e.fd.value === ''? 0 : parseInt(e.fd.value),
                    "medical-insurance": e.medical.value === ''? 0 : parseInt(e.medical.value),
                    "ppf": e.ppf.value === ''? 0 : parseInt(e.ppf.value),
                    "home-loan-principal": e.home.value === ''? 0 : parseInt(e.home.value)
                }
            }
        }
        axios.post('http://35.219.166.177:8000/taxPayer', data)
            .then(response => {
                if (response.status === 201) { 
                   document.getElementById('name').value = ''
                   document.getElementById('pan').value = ''
                   document.getElementById('email').value = ''
                   document.getElementById('ppf').value = ''
                   document.getElementById('med').value = ''
                   document.getElementById('home').value = ''
                   document.getElementById('fd').value = ''
                   document.getElementById('funds').value = ''
                    document.getElementById('insurance').value = ''
                    setMsg('Registered Successfully')
                }
                else {
                    setMsg('User already exists')
                }
            })
    }
    return (
        <>
            <Navbar expand="lg" bg="primary" variant="dark">
                <Container style={{ marginLeft: "5px" }}>
                    <Navbar.Brand>Tax-filing</Navbar.Brand>
                    <Nav className="me-auto">
                        <Nav.Link href="/secure">Register New User</Nav.Link>
                        <Nav.Link href="/secure/verify">Verify Investments</Nav.Link>
                        <Nav.Link href="/secure/update">Update Investments</Nav.Link>
                    </Nav>
                </Container>
            </Navbar>
            <div className='content'>
                <h1>Register New User</h1>
                <Form className='form' onSubmit={register}>
                    <h3>Personal Information</h3>
                    <div className='input'>
                        <Form.Group className='input-fields' class='input-field' controlId="name">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" name="name" placeholder="Enter name" />
                        </Form.Group>
                        <Form.Group className='input-fields' class='input-field' controlId="pan">
                            <Form.Label>PAN</Form.Label>
                            <Form.Control type="text" name="pan" placeholder="Enter PAN" />
                        </Form.Group>
                        <Form.Group className='input-fields' class='input-field' controlId="email">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" name="email" placeholder="Enter email" />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                    </div>
                    <div className='input'>
                        <Form.Group className='input-fields' class='input-field' controlId="insurance">
                            <Form.Label>Insurance</Form.Label>
                            <Form.Control name="insurance" type="number" />
                        </Form.Group>
                        <Form.Group className='input-fields' class='input-field' controlId="fd">
                            <Form.Label>Fixed Deposit</Form.Label>
                            <Form.Control name="fd" type="number" />
                        </Form.Group>
                        <Form.Group className='input-fields' class='input-field' controlId="med">
                            <Form.Label>Medical Insurance</Form.Label>
                            <Form.Control name="medical" type="number" />
                        </Form.Group>
                        <Form.Group className='input-fields' class='input-field' controlId="ppf">
                            <Form.Label>PPF</Form.Label>
                            <Form.Control name="ppf" type="number" />
                        </Form.Group>
                        <Form.Group className='input-fields' class='input-field' controlId="funds">
                            <Form.Label>Mutual Funds</Form.Label>
                            <Form.Control name="funds" type="number" />
                        </Form.Group>
                        <Form.Group className='input-fields' class='input-field' controlId="home">
                            <Form.Label>Home Loan Principal</Form.Label>
                            <Form.Control name="home" type="number" />
                        </Form.Group>
                    </div>
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                    <h1>{msg}</h1>
                </Form>
            </div>
        </>
    )
}


export default Secure