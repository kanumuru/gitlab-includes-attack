resource "aws_s3_bucket" "gitlab" {
  bucket = format("gitlab-pipeline-%s",random_string.random_name.result)
}


resource "aws_s3_object" "gitlab" {
  bucket = aws_s3_bucket.gitlab.bucket
  key = "pipeline.yml"
  source = "pipeline.yml"
}

resource "aws_s3_bucket_policy" "policy" {
    depends_on = [aws_s3_bucket.gitlab,aws_s3_bucket_ownership_controls.foo,aws_s3_bucket_public_access_block.foo1 ]

  bucket = aws_s3_bucket.gitlab.id

  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[{
	"Sid":"PublicReadGetObject",
        "Effect":"Allow",
	  "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::${aws_s3_bucket.gitlab.bucket}/*"
      ]
    }
  ]
}
POLICY
}

resource "aws_s3_bucket_acl" "gitlab" {
  bucket = aws_s3_bucket.gitlab.id
  acl    = "public-read"
  depends_on = [aws_s3_bucket.gitlab,aws_s3_bucket_ownership_controls.foo,aws_s3_bucket_public_access_block.foo1 ]
}

resource "aws_s3_bucket_ownership_controls" "foo" {
  bucket = aws_s3_bucket.gitlab.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
  depends_on = [ aws_s3_bucket.gitlab ]
}

resource "aws_s3_bucket_public_access_block" "foo1" {
  bucket = aws_s3_bucket.gitlab.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}


output "Bucketinfo" {
  value = {
      BucketName = aws_s3_bucket.gitlab.bucket
  }
}


resource "null_resource" "gitlab" {
    depends_on = [
      aws_s3_bucket.gitlab
    ]
  provisioner "local-exec" {
    command = "echo include: https://${aws_s3_bucket.gitlab.id}.s3.us-west-2.amazonaws.com/pipeline.yml >> /root/gitlab-includes-attack/.gitlab-ci.yml"
  }
}
